#!/usr/bin/python

import sys
import subprocess

if len(sys.argv) < 3:
    print "Wrong command line. Usage: %s <kmers_file> <jf_index>" % (sys.argv[0])
    exit(1)

kmers_file = sys.argv[1]
jf_index = sys.argv[2]
jf_bin = "/home/smokesun/Disser/Soft/jellyfish-bin/bin/jellyfish"

kmers_num = 0
occur_num = 0

with open(kmers_file, "rt") as infile:
    for line in infile:
        if line[0] == '>':
            continue
        kmer = line.strip()
        kmers_num += 1
        jf_cmd = jf_bin +  " query " + jf_index + " " +  kmer
        proc = subprocess.Popen(jf_cmd, shell=True, stdout=subprocess.PIPE)
        res = int(proc.stdout.read().strip().split(" ")[1])
        if res == 1:
            occur_num += 1

print "Kmers num = %d; Occur num = %d; Occur percentage = %f" %(kmers_num, occur_num, float(occur_num) / kmers_num)
