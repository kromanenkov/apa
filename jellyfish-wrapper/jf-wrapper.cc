/*  This file is part of Jellyfish.

    Jellyfish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jellyfish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Jellyfish.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>

#include <jellyfish/err.hpp>
#include <jellyfish/thread_exec.hpp>
#include <jellyfish/file_header.hpp>
#include <jellyfish/stream_manager.hpp>
#include <jellyfish/mer_overlap_sequence_parser.hpp>
#include <jellyfish/mer_iterator.hpp>
#include <jellyfish/mer_dna_bloom_counter.hpp>
#include <jellyfish/fstream_default.hpp>
#include <jellyfish/jellyfish.hpp>
#include <sub_commands/query_main_cmdline.hpp>

namespace err = jellyfish::err;

using jellyfish::mer_dna;
using jellyfish::mer_dna_bloom_counter;
typedef std::vector<const char*> file_vector;
typedef jellyfish::mer_overlap_sequence_parser<jellyfish::stream_manager<file_vector::iterator> > sequence_parser;
typedef jellyfish::mer_iterator<sequence_parser, mer_dna> mer_iterator;

static query_main_cmdline args;

// mer_dna_bloom_counter query_load_bloom_filter(const char* path) {
//   return res;
// }

template<typename PathIterator, typename Database>
void query_from_sequence(PathIterator file_begin, PathIterator file_end, const Database& db,
                         std::ostream& out, bool canonical) {
  jellyfish::stream_manager<PathIterator> streams(file_begin, file_end);
  sequence_parser parser(mer_dna::k(), 1, 3, 4096, streams);
  for(mer_iterator mers(parser, canonical); mers; ++mers)
    out << *mers << " " << db.check(*mers) << "\n";
}

// Specify running states of jellyfish wrapper.
enum RunningMode {
  // Print only unique kmers stat.
  OutputUnique = 0,
  // Print only repeated kmers stat.
  OutputRepeated = 1,
  // Print only present kmers stat.
  OutputPresent = 2,
  // Print missing kmers stat.
  OutputMissing = 3,
  // Print all stat.
  OutputAll = 4
};

template<typename PathIterator, typename Database>
void my_query_from_sequence(PathIterator file_begin, PathIterator file_end, const Database& db,
                            std::ostream& out, bool canonical, const RunningMode Mode,
                            unsigned long int KmersNum) {
  jellyfish::stream_manager<PathIterator> streams(file_begin, file_end);
  sequence_parser parser(mer_dna::k(), 1, 3, 4096, streams);

  // Progress statistics will be printed after processing another (1 / Step) %
  // kmers portion.
  const static double Step = 0.1;
  const unsigned int PercentageStep = 100 * Step;
  unsigned int PercentageProcessed = 0;
  unsigned long int TotalMersNum = 0, PresentMersNum = 0, UniqMersNum = 0,
                    MissingMersNum = 0, RepeatedMersNum = 0,
                    ProgressBatch = KmersNum * Step, NextCheckpoint = 0;

  std::cout << KmersNum << " kmers will be processed.\n" << "\n";

  // Array with kmers occurences.
  const int MaxOccurence = 100;
  unsigned long int KmersOccurence[MaxOccurence];
  std::fill(KmersOccurence, KmersOccurence + MaxOccurence, 0);

  for (mer_iterator mers(parser, canonical); mers; ++mers) {
    ++TotalMersNum;
    if (TotalMersNum > NextCheckpoint) { // Print next progress statistics.
      std::cout << PercentageProcessed << "% of kmers are processed..\n";
      PercentageProcessed += PercentageStep;
      NextCheckpoint += ProgressBatch;
    }

    unsigned int CheckRes = db.check(*mers);
    CheckRes += db.check(mers->get_reverse_complement());

    if (CheckRes >= MaxOccurence)
      continue;

    ++KmersOccurence[CheckRes];

    if (CheckRes == 1) {
      ++UniqMersNum;
      ++PresentMersNum;
    } else {
      if (CheckRes == 0)
        ++MissingMersNum;
      else {
        ++PresentMersNum;
        ++RepeatedMersNum;
      }
    }
  }

  double PresentRate = double(PresentMersNum) / TotalMersNum * 100,
         RepeatedRate = double(RepeatedMersNum) / TotalMersNum * 100,
         UniqRate = double(UniqMersNum) / TotalMersNum * 100,
         MissingRate = double(MissingMersNum) / TotalMersNum * 100;

  switch (Mode) {
    case OutputUnique:
      std::cout << TotalMersNum << " " << UniqMersNum << " " << std::setprecision(8) << UniqRate << "\n";
      break;
    case OutputRepeated:
      std::cout << TotalMersNum << " " << RepeatedMersNum << " " << std::setprecision(8) << RepeatedRate << "\n";
      break;
    case OutputPresent:
       std::cout << TotalMersNum << " " << PresentMersNum << " " << std::setprecision(8) << PresentRate << "\n";
      break;
    case OutputMissing:
      std::cout << TotalMersNum << " " << MissingMersNum << " " << std::setprecision(8) << MissingRate << "\n";
      break;
    case OutputAll: {

      std::cout << std::setprecision(8) << "Total " << TotalMersNum << " kmers tested.\n"
                << "Present: " << PresentMersNum << " (" << PresentRate << "%)\n"
                << "Uniq: " << UniqMersNum << " (" << UniqRate << "%)\n"
                << "Missing: " << MissingMersNum << " (" << MissingRate << "%)\n";

      int i = MaxOccurence - 1;
      // Find last non-null kmers occurence.
      for (; i >= 0; --i)
        if (KmersOccurence[i] != 0)
          break;

      std::cout << std::setprecision(8);
      // Print kmers occurences until last non-null.
      for (int j = 0; j <= i; ++j) {
        std::cout << j << " : " << KmersOccurence[j] << " : (" << double(KmersOccurence[j]) / TotalMersNum << ")\n";
      }

      break;
    }
    default:
      std::cout << "Unknown running mode. Exit now.\n";
      return;
  }
}

template<typename Database>
void query_from_cmdline(std::vector<const char*> mers, const Database& db, std::ostream& out,
                        bool canonical) {
  mer_dna m;
  for(auto it = mers.cbegin(); it != mers.cend(); ++it) {
    try {
      m = *it;
      if(canonical)
        m.canonicalize();
      out << m << " " << db.check(m) << "\n";
    } catch(std::length_error e) {
      std::cerr << "Invalid mer '" << *it << "'\n";
    }
  }
}

template<typename Database>
void query_from_stdin(const Database& db, std::ostream& out, bool canonical) {
  std::string buffer;
  mer_dna     m;

  while(getline(std::cin, buffer)) {
    try {
      m = buffer;
      if(canonical)
        m.canonicalize();
      out << db.check(m) << std::endl;  // a flush is need for interactive use
    } catch(std::length_error e) {
      std::cerr << "Invalid mer '" << buffer << "'" << std::endl;
    }
  }
}

// Running mode should be passed as the argv[1] parameter therefore not
// broking Jellifish arguments parsing function.
RunningMode get_mode(int argc, char *argv[]) {
  // Print all by default.
  RunningMode Mode = OutputAll;

  if (strcmp(argv[1], "--unique") == 0)
    Mode = OutputUnique;
  else if (strcmp(argv[1], "--repeated") == 0)
    Mode = OutputRepeated;
  else if (strcmp(argv[1], "--missing") == 0)
    Mode = OutputMissing;
  else if (strcmp(argv[1], "--all") == 0)
    Mode = OutputAll;
  else
    // The following increment will keep the original optind value if the running mode
    // is not specified. In other cases optind calue will be incremented.
    --optind;

  ++optind;
  return Mode;
}

// Count number of kmers in tested file. This number is used to print progress info.
unsigned long int get_kmers_num(const char *filepath) {
  std::ifstream KmersFile(filepath);
  unsigned long int KmersNum = 0;

  if (!KmersFile.good())
    err::die(err::msg() << "Error opening file with kmers '" << filepath << "'\n");

  std::string line;
  while (std::getline(KmersFile, line))
    if (!line.empty() && line[0] != '>')
      ++KmersNum;

  return KmersNum;
}

int main(int argc, char *argv[])
{
  // Number of kmers in tested file for progress info.
  unsigned long int KmersNum = 0;
  // Get running mode from args, increment optind.
  RunningMode Mode = get_mode(argc, argv);

  args.parse(argc, argv);

  ofstream_default out(args.output_given ? args.output_arg : 0, std::cout);
  if(!out.good())
    err::die(err::msg() << "Error opening output file '" << args.output_arg << "'");

  std::ifstream in(args.file_arg, std::ios::in|std::ios::binary);
  jellyfish::file_header header(in);

  if(!in.good())
    err::die(err::msg() << "Failed to parse header of file '" << args.file_arg << "'");

  mer_dna::k(header.key_len() / 2);

  if (header.format() == binary_dumper::format) {
    jellyfish::mapped_file binary_map(args.file_arg);

    if (!args.no_load_flag &&
       (args.load_flag || (args.sequence_arg.begin() != args.sequence_arg.end()) || (args.mers_arg.size() > 100)))
      binary_map.load();
    binary_query bq(binary_map.base() + header.offset(), header.key_len(), header.counter_len(), header.matrix(),
                               header.size() - 1, binary_map.length() - header.offset());

    std::cout << "Starting Q score calculation.\n";
    // We suppose that there is only one input file with kmers for testing.
    if (args.sequence_arg.size() != 1)
      err::die(err::msg() << "Please provide exactly one file with kmers (" << args.sequence_arg.size() << " were"
                          << " provided).");
    KmersNum = get_kmers_num(args.sequence_arg.front());
    my_query_from_sequence(args.sequence_arg.begin(), args.sequence_arg.end(), bq, out, header.canonical(), Mode, KmersNum);
  } else {
    err::die(err::msg() << "Unsupported format '" << header.format() << "'. Must be a bloom counter or binary list.");
  }

  return 0;
}

