#!/usr/bin/env python2.7

import argparse
import os
import sys
import shutil
import logging
import tempfile
import contextlib
import multiprocessing as mp

from jf_helper import Wrapper, JFWrapper, CountKmersWrapper
import utils

# TODO: Create a proper config file for these paths.
# Hardcoded name of the jellyfish wrapper. It should be built before running
# this script.
count_kmers_path = os.path.join('jellyfish-wrapper', 'count_kmers')
# Hardcoded name of the helper script for removing contigs shorter than the
# specified length.
remove_small_path = os.path.join('scripts', 'remove_small.pl')

# Some global constants for intermediate files.

# Jellyfish database of reads kmers name.
bd_name = 'reads_db'
# File with the kmers frequencies in reads.
freq_name = 'freq.txt'
# Filename prefix for assemblies without short contigs.
assembly_prefix = 'contigs_'
# Filename suffix for assembly database.
assembly_db_suffix = '_db'
# Filename prefix for reads kmers in selected neighbourhood.
nhood_prefix = 'reads_kmers_'


def expand_and_abs(path):
    """ Resolve '~' symbol and relative path. """
    try:
        if not os.path.exists(path):
            return path
        return os.path.abspath(os.path.expanduser(path))
    except Exception:
        return None


class AssemblyResult(object):
    def __init__(self, path):
        self.path = path
        (self.dirname, self.filename) = os.path.split(path)
        # A short name which will be printed instead of full filepath.
        self.shortcut = self.filename
        self.size = utils.get_fasta_size(path)


@contextlib.contextmanager
def init_tempdir():
    """ Create temporary directory for storing intermediate files like
        Jellyfish databases, kmers frequences and so on. This directory
        will be removed at the close of script. """
    name = tempfile.mkdtemp()
    try:
        yield name
    finally:
        shutil.rmtree(name)
        # pass


# Main class with the major logic.
class Apa(object):
    def __init__(self, jf_path, reads_list, k, min_len, reads_db, histo_file,
                 reads_kmers):
        # Path to Jellyfish executable.
        self.jf_path = expand_and_abs(jf_path)
        apa_path = os.path.dirname(os.path.realpath(__file__))
        # Path to count_kmers executable.
        self.count_kmers_path = os.path.join(apa_path, count_kmers_path)
        # Path to remove_small.pl script.
        self.remove_small_path = os.path.join(apa_path, remove_small_path)
        # List of AssemblyResult objects.
        self.assemblies_list = []
        # List of files with reads.
        self.reads_list = [expand_and_abs(f) for f in reads_list]
        # Kmer length.
        self.k = k
        # Min contig length threshold.
        self.min_len = min_len
        # Jellyfish database for kmers in reads.
        self.db = expand_and_abs(reads_db)
        # File with kmers frequencies in reads.
        self.histo_file = expand_and_abs(histo_file)
        self.reads_kmers = expand_and_abs(reads_kmers)
        print "\nWelcome to Assembly Parameter Advisor.\n"

    def update_shortcuts(self):
        """ Make shortcuts of assemblies unique by iteratively prepending them
            by parts of filepaths. If now succeeded full filepath become a
            shortcut. """
        # Copy of assemblies_list. Modify paths inside this copy.
        list_copy = self.assemblies_list[:]
        # A flag to check that we could extend another shortcut.
        can_extend = True

        # Iteratively try to prepend the current shortcut by the filepath part.
        shortcuts_set = {i.shortcut for i in list_copy}
        while len(shortcuts_set) != len(list_copy) and can_extend:
            for i, item in enumerate(list_copy):
                (new_dirname, to_append) = os.path.split(item.dirname)
                if new_dirname == item.dirname:
                    # Can't split path anymore.
                    can_extend = False
                    break
                list_copy[i].dirname = new_dirname
                list_copy[i].filename = os.path.join(to_append, item.filename)

            shortcuts_set = {i.filename for i in list_copy}

        # Update shorcuts in AssemblyResult objects.
        for i, item in enumerate(self.assemblies_list):
            self.assemblies_list[i].shortcut =\
                list_copy[i].filename if can_extend else \
                os.path.join(list_copy[i].dirname, list_copy[i].filename)

    def parse_assemblies(self, files_list):
        """ From a list of AssemblyResult objects from the list of files.
            Make paths absolute. """
        # Resolve '~' symbol and relative paths.
        files_list = [expand_and_abs(i) for i in files_list
                      if i.endswith('.fa') or i.endswith('.fasta')]

        if not files_list:
            print "No input assemblies or all of them are in wrong format. "\
                  "Exit now."
            sys.exit(0)

        print 'Forming list of input assemblies..',
        try:
            for file in files_list:
                self.assemblies_list.append(AssemblyResult(file))
        except (IOError, ValueError):
            print "Problems while parsing assemblies filepaths. Exit now."
            sys.exit(0)
        print 'Done.\n'

        # Print input assemblies.
        print "These assemblies will be evaluated:"
        for file in files_list:
            print file
        print

        # Update shortcut names for assemblies to make them unique.
        self.update_shortcuts()

    def print_files_with_reads(self):
        """ Simply print the list of input files with reads. """
        print "File(s) with reads for evaluating assemblies:"
        for file in self.reads_list:
            print file
        print

    def process_reads(self, tmp_dir):
        """ Create all necessary files for user-provided reads. All work is
            done in the temporary directory. """
        jf_wrapper = JFWrapper(self.jf_path, tmp_dir)

        if not self.db:
            # Path to generated database.
            db = os.path.join(tmp_dir, bd_name)

            print "Indexing kmers in reads. Could take some time..",
            # Create a canonical Jellyfish database for kmers in reads.
            (output, err) = jf_wrapper.count(self.k, db, self.reads_list,
                                             canonical=True)
            print "Done."

            # Save path to generated db.
            self.db = db
        else:
            print 'Found Jellyfish database ({}), skip indexing kmers in '\
                  'reads.'.format(self.db)

        if not self.histo_file:
            # Path to generated file with kmers frequencies.
            histo_file = os.path.join(tmp_dir, freq_name)

            # Produce histogram of kmers frequencies.
            (output, err) = jf_wrapper.histo(self.db, histo_file)

            # Store generated histogram.
            self.histo_file = histo_file
        else:
            print 'Found histogram file ({}), skip histogram creating.'.\
                format(self.histo_file)

    def process_assembly(self, assembly, tmp_dir):
        """ Create all necessary files for the assembly. All work is done in
            the temporary directory. """
        # Assembly index.
        index = self.assemblies_list.index(assembly)
        # Filtered out contigs shorter than threshold.
        filtered_contigs =\
            os.path.join(tmp_dir, assembly_prefix + '{}.fa'.format(index))

        filter_wrapper = Wrapper(self.remove_small_path, tmp_dir)
        args = [self.min_len, assembly.path]

        # Filter out short contigs.
        with open(filtered_contigs, "w") as filtered_file:
            (output, err) =\
                filter_wrapper.execute(args, my_stdout=filtered_file)

        jf_wrapper = JFWrapper(self.jf_path, tmp_dir)

        # Path to generated database.
        db = os.path.join(tmp_dir, str(index) + assembly_db_suffix)

        # Create a Jellyfish database for kmers in assembly.
        (output, err) = jf_wrapper.count(self.k, db, [filtered_contigs])

        # Save path to generated db.
        assembly.db = db

        # Get number of unique kmers in assembly.
        (output, err) = jf_wrapper.histo(db)
        (freq, num) = output.splitlines()[0].split()
        if int(freq) != 1:
            raise ValueError("Unique kmers doesn't appear first. Looks like "
                             "this Jellyfish version uses other format.")
        assembly.uniq_kmers_num = int(num)

    def process_histo(self, tmp_dir):
        """ Analyze kmers frequency histogram to estimate genome size and
            kmers coverage peak. """
        if self.reads_kmers:
            self.kmers_file = self.reads_kmers
            print 'Found file with kmers ({}), skip reads dumping.'.\
                format(self.kmers_file)
        else:
            histo_analyzer = utils.HistoAnalyzer(self.histo_file)
            # Get estimated genome size.
            genome_size = histo_analyzer.estimate_genome_size()

            size_list = [assembly.size for assembly in self.assemblies_list]
            # Find assembly with size closest to estimated genome size.
            assembly_id = utils.find_nearest(size_list, genome_size)
            # Get number of unique kmers from this assembly.
            uniq_kmers_num = self.assemblies_list[assembly_id].uniq_kmers_num

            (l_border, r_border) =\
                histo_analyzer.calculate_nhood(uniq_kmers_num)

            # Path to file with kmers in selected neighbourhood.
            kmers_file = os.path.join(
                tmp_dir, nhood_prefix + '{}_{}.fa'.format(l_border, r_border))

            jf_wrapper = JFWrapper(self.jf_path, tmp_dir)
            # Dump reads kmers from the neighbourhood.
            (output, err) = jf_wrapper.dump(self.db, kmers_file, str(l_border),
                                            str(r_border))

            # Store generated file with kmers.
            self.kmers_file = kmers_file

    def evaluate_assembly(self, assembly, tmp_dir, queue):
        """ Calculate Q score for each assembly and some additional
            statistics if asked. Put modified AssemblyResult object to the
            queue."""
        # count_kmers wrapper.
        ck_wrapper = CountKmersWrapper(self.count_kmers_path, tmp_dir)

        print 'Evaluating {} assembly..'.format(assembly.shortcut)
        (output, err) = ck_wrapper.execute(self.kmers_file, assembly.db)
        for line in output.splitlines():
            splitted = line.split()
            if not len(splitted):
                continue

            float_in_str = utils.extract_float_from_str(line)
            float_num = float(float_in_str) if float_in_str else None
            # Save frequencies from count_kmers output.
            if splitted[0] == 'Present:':
                assembly.present_freq = float_num
            elif splitted[0] == 'Uniq:':
                assembly.uniq_freq = float_num
            elif splitted[0] == 'Missing:':
                assembly.missing_freq = float_num
        print 'Done with {}.'.format(assembly.shortcut)

        queue.put(assembly)

    def print_results(self):
        """ Prints all evaluation results. """
        # Make copy of a list with AssemblyResult.
        assemblies_list = self.assemblies_list[:]
        assemblies_list.sort(key=lambda x: x.uniq_freq, reverse=True)

        print '\n====== Evaluation results ======'
        for assembly in assemblies_list:
            print '{} assembly:'.format(assembly.shortcut)
            print 'Uniq: {}; Present: {}; Missing: {}; Size: {}'.format(
                assembly.uniq_freq, assembly.present_freq,
                assembly.missing_freq, assembly.size)
            print

    def evaluate(self):
        """ Main program logic. """
#        try:
        with init_tempdir() as tmp_dir:
            # Create all necessary files for the evaluation.
            self.process_reads(tmp_dir)
            for assembly in self.assemblies_list:
                self.process_assembly(assembly, tmp_dir)

            # Analyze the histogram of reads kmers.
            self.process_histo(tmp_dir)

            # Evaluate assemblies.
            # Create queues for return values and a pool of processes.
            queues = [mp.Queue() for i in self.assemblies_list]
            procs = [mp.Process(target=self.evaluate_assembly, args=(
                     self.assemblies_list[i], tmp_dir, queues[i]))
                     for i, elem in enumerate(self.assemblies_list)]

            # Start processes.
            for proc in procs:
                proc.start()
            # Replace init assemblies list with modified list.
            self.assemblies_list = [q.get() for q in queues]
            # TODO: Some manuals warn against using join() while writing to
            # queue. Investigate it.
            for proc in procs:
                proc.join()

            # Output results.
            self.print_results()

#        except Exception as e:
#            print "\n" + e
#            print "An error occured during the evaluation process. Exit now."
#            sys.exit(1)


def get_input_files_list(input_dir):
    """ Form a list of all *.fa and *.fasta files in the specified
        directory """
    # Resulted list with files.
    result = []

    # Resolve '~' and relative path.
    abs_dir_path = expand_and_abs(input_dir)

    # Check if the path to input directory is correct.
    if not os.path.isdir(abs_dir_path):
        print "{} is not a correct directory path. Exit now.".\
            format(abs_dir_path)
        sys.exit(1)

    # Read fasta files from specified directory and add them to the
    # args.files list.
    for f in os.listdir(abs_dir_path):
        if f.endswith('.fa') or f.endswith('.fasta'):
            result.append(os.path.join(abs_dir_path, f))

    return result


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Tool for evaluating de \
                                                  novo NGS assemblies based \
                                                  on the kmers frequencies. \
                                                  You could specify either \
                                                  the directory with \
                                                  assemblies or a list of \
                                                  assemblies.')
    parser.add_argument('-d', '--dir', default='', help='Path to directory \
                                                         with assemblies.')
    parser.add_argument('-k', default='21',
                        help='Kmer length. Default is 21.')
    parser.add_argument('-min_len', default='500',
                        help='Contigs shorter than this threshold will be \
                              removed from the assemblies before evaluation. \
                              Default is 500.')
    parser.add_argument('--reads_db', help='Precomputed Jellyfish database \
                                            for kmers in reads.')
    # Flag for debug.
    parser.add_argument('--histo_file', help='Precomputed occurence histogram \
                                              for kmers in reads.')
    # Flag for debug.
    parser.add_argument('--reads_kmers', help="Precomputed file with kmers \
                                               from the second peak's \
                                               neighbourhood in the occurence \
                                               histogram for kmers in reads.")
    parser.add_argument('files', nargs='*',
                        help='List of assemblies.')
    parser.add_argument('--jf_path', nargs=1, required=True,
                        help='Path to Jellyfish executable. Mandatory \
                              parameter.')
    parser.add_argument('-r', '--reads', nargs='+', required=True,
                        help='Path to reads used for assemblies. Mandatory \
                              parameter.')
    args = parser.parse_args()

    if args.dir:
        # Defence check if the user specified both path to directory and a list
        # of assemblies.
        if args.files:
            print "Please specify either directory or a list of assemblies, "\
                  "not both. Exit now."
            sys.exit(1)

        # Add all *.fa and *.fasta files in directory to the list.
        args.files = get_input_files_list(args.dir)

    apa = Apa(args.jf_path[0], args.reads, args.k, args.min_len, args.reads_db,
              args.histo_file, args.reads_kmers)
    apa.parse_assemblies(args.files)
    apa.print_files_with_reads()
    apa.evaluate()
