#!/usr/bin/env python2.7

import multiprocessing
from subprocess import Popen, PIPE


# Basic class for program wrapper.
class Wrapper(object):
    def __init__(self, bin_path, tmp_dir):
        # Program path
        self.bin_path = bin_path
        # Temporaty directory.
        self.tmp_dir = tmp_dir

    def execute(self, args, my_stdout=PIPE, my_stderr=PIPE):
        """ Run program with specified arguments. """
        run_cmd = [self.bin_path] + args

        print "CMD: {}".format(run_cmd)

        p = Popen(run_cmd, stdout=my_stdout, stderr=my_stderr)
        (output, err) = p.communicate()
        if err:
            raise ValueError(err)

        return (output, err)


# Wrapper around Jellyfish program.
class JFWrapper(Wrapper):
    def __init__(self, jf_path, tmp_dir):
        super(JFWrapper, self).__init__(jf_path, tmp_dir)
        self.jf_path = jf_path

    def count(self, k, db, reads_list, canonical=False):
        """ jellyfish count wrapper. """
        # Threads number.
        threads = multiprocessing.cpu_count()

        args = ['count', '-m', k, '-s', '100M', '-t', str(threads), '-o', db]
        if canonical:
            args.append('-C')
        if len(reads_list) > 1:
            # In case of multiple input files they could be opened in parallel.
            parallel_opened_files = min(len(reads_list), threads)
            args.extend(['-F', str(parallel_opened_files)])

        args.extend(reads_list)
        return self.execute(args)

    def histo(self, db, histo_file=''):
        """ jellyfish histo wrapper. """
        args = ['histo']
        if histo_file:
            args += ['-o', histo_file]
        args.append(db)

        return self.execute(args)

    def dump(self, db, kmers_file, L, U):
        """ jellyfish dump wrapper. """
        args = ['dump', '-L', L, '-U', U, '-o', kmers_file, db]

        return self.execute(args)


# Wrapper around count_kmers program.
class CountKmersWrapper(Wrapper):
    def __init__(self, ck_path, tmp_dir):
        super(CountKmersWrapper, self).__init__(ck_path, tmp_dir)
        self.ck_path = ck_path

    def execute(self, kmers_file, db):
        args = ['-s', kmers_file, db]

        return super(CountKmersWrapper, self).execute(args)
