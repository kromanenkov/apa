#!/usr/bin/env python2.7

import re
import numpy as np
from scipy.signal import argrelmax, argrelmin

# Required percentage of kmers in neighbourhood.
kmers_percentage = 1.1


# Different methods based on kmers histogram analysis.
class HistoAnalyzer(object):
    def __init__(self,  histo_file):
        self.histo_file = histo_file

        # Load matches and occurencies arrays from Jellyfish histogram.
        (self.match, self.occurence) =\
            np.loadtxt(histo_file, unpack=True, dtype=int)
        # Get rid of 0-match kmers in histogram.
        if (self.match[0] == 0):
            self.match = self.match[1:]
            self.occurence = self.occurence[1:]

    def get_local_max_id(self):
        """ Find id of the second peak on the histogram. """
        try:
            return self.local_max_id
        except AttributeError:
            self.local_maxs = argrelmax(self.occurence, order=8)[0]

            local_maxs_len = len(self.local_maxs)
            if local_maxs_len == 0 or\
               (self.local_maxs[0] == 0 and local_maxs_len == 1):
                raise ValueError("Looks like the reads kmers histogram "
                                 "doesn't have the second peak. Please "
                                 "reinvestigate your data.")

            self.local_max_id = self.local_maxs[0] if self.local_maxs[0]\
                else self.local_maxs[1]

            return self.local_max_id

    def get_local_min_id(self, first_peak_id=0):
        """ Find id of the local minimum between two peaks. """
        try:
            return self.local_min_id
        except AttributeError:
            second_peak_id = self.get_local_max_id()
            self.local_mins =\
                argrelmin(self.occurence[first_peak_id:second_peak_id],
                          order=5)[0]

            if len(self.local_mins) == 0:
                raise ValueError("Looks like the reads kmers histogram "
                                 "doesn't have the minimum between two peaks. "
                                 "Please reinvestigate your data.")

            self.local_min_id = self.local_mins[0]

            return first_peak_id + self.local_min_id

    def estimate_genome_size(self):
        """ Estimate genome size from kmers occurencies. """
        try:
            return self.size
        except AttributeError:
            # Get ids of two local peaks.
            local_max_id = self.get_local_max_id()
            local_min_id = self.get_local_min_id()

            size = 0
            # Estimate size as the sum of match[i] * occurence[i] divided by
            # match[local_max_id].
            for i in range(local_min_id, len(self.match)):
                size += self.match[i] * self.occurence[i]

            self.size = size / self.match[local_max_id]

            return self.size

    def calculate_nhood(self, kmers_number):
        """ Calculate the neighbourhood of the second peak in histogram. """
        # Get ids of two local peaks.
        local_max_id = self.get_local_max_id()
        local_min_id = self.get_local_min_id()

        # Right border.
        r_border_id = (local_max_id - 10) * 2
        # Twice match number for the local_max_id.
        twice_peak_match = (self.match[local_max_id] - 10) * 2

        # Shift border if doubled peak match is absent in self.match array.
        if self.match[r_border_id] > twice_peak_match:
            while self.match[r_border_id] > twice_peak_match:
                r_border_id -= 1
#        elif self.match[r_border_id] < twice_peak_match:
#            while self.match[r_border_id] < twice_peak_match:
#                r_border_id += 1

        # Target number of kmers in neighbourhood.
        target_num = int(kmers_percentage * kmers_number)
        # Actual kmers num.
        actual_num = 0

        l_border_id = r_border_id
        # Shift left border until it bumps into local_min_id or we reach the
        # target kmers num.
        while (l_border_id >= local_min_id - 10 and
               ((actual_num / self.match[local_max_id]) < target_num)):
            actual_num += self.match[l_border_id] * self.occurence[l_border_id]
            l_border_id -= 1

        return (self.match[l_border_id], self.match[r_border_id])


def get_fasta_size(path):
    """ Calculate the number of nucleotides symbols in FASTA file. """
    size = 0

    with open(path) as file:
        for line in file:
            if line.startswith(">"):  # FASTA header.
                continue
            size += len(line.strip())

    return size


def find_nearest(mylist, num):
    """ Find closest to num element from list. """
    return (np.abs(np.array(mylist) - num)).argmin()


def extract_float_from_str(s):
    """ Extract float number from the string s. """
    res = re.findall('\d+\.\d+', s)
    if res:
        return res[0]
    else:
        return None
