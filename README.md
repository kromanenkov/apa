#APA - Assembly Pipeline Advisor.

Tool for evaluating de novo NGS assemblies based on the kmers frequencies. You could specify either the directory with assemblies or a list of assemblies.

APA depends on Jellyfish software which should be installed as follows:

1. Download and build Jellyfish (Tested on 2.2.4 and 2.2.6 versions).
2. Copy all libs from jellyfish-install-dir/lib/ into apa-install-dir/jellyfish-wrapper/lib (or make a symlink).
3. cd into apa-install-dir/jellyfish-wrapper and run `make`
4. This will produce a count\_kmers executable inside apa-install-dir/jellyfish-wrapper.

```
usage: apa.py [-h] [-d DIR] [-k K] [-min_len MIN_LEN] [--reads_db READS_DB]
              [--histo_file HISTO_FILE] [--reads_kmers READS_KMERS] --jf_path
              JF_PATH -r READS [READS ...]
              [files [files ...]]

positional arguments:
  files                 List of assemblies.

optional arguments:
  -h, --help            show this help message and exit
  -d DIR, --dir DIR     Path to directory with assemblies.
  -k K                  Kmer length. Default is 21.
  -min_len MIN_LEN      Contigs shorter than this threshold will be removed
                        from the assemblies before evaluation. Default is 500.
  --reads_db READS_DB   Precomputed Jellyfish database for kmers in reads.
  --histo_file HISTO_FILE
                        Precomputed occurence histogram for kmers in reads.
  --reads_kmers READS_KMERS
                        Precomputed file with kmers from the second peak's
                        neighbourhood in the occurence histogram for kmers in
                        reads.
  --jf_path JF_PATH     Path to Jellyfish executable. Mandatory parameter.
  -r READS [READS ...], --reads READS [READS ...]
                        Path to reads used for assemblies. Mandatory
                        parameter
```
